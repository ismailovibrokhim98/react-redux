import React, { useState } from "react";
import { useSelector } from "react-redux";

const Counter = () => {
  const [count, setCount] = useState(0);
  const reduxCount = useSelector((state) => state.counter.count);
  // ! IGNORE THE CODE BELOW
  const addCode = "onClick={() => setCount(count + 1)}";
  const delCode = "onClick={() => setCount(count - 1)}";
  const resCode = "onClick={() => setCount(0)}";
  // ! IGNORE THE CODE ABOVE

  return (
    <div className="Counter">
      <h1>Counter Component</h1>
      {/* // ! DON'T PAY ATTENTION */}
      <pre>
        State: <code>const [count, setCount] = useState({count});</code>
      </pre>
      <pre>
        Child: <code>None</code>
      </pre>
      {/* // ! DON'T PAY ATTENTION */}
      <hr />
      <p>My count is: {count}</p>
      <p>Redux count is: {reduxCount}</p>
      <button className="btn" onClick={() => setCount(count + 1)}>
        ADD
      </button>
      <button className="btn" onClick={() => setCount(count - 1)}>
        DEL
      </button>
      <button className="btn" onClick={() => setCount(0)}>
        RESET
      </button>
      <hr />
      {/* // ! DON'T PAY ATTENTION */}
      <pre>
        ADD: <code>{addCode}</code>
      </pre>
      <pre>
        DEL: <code>{delCode}</code>
      </pre>
      <pre>
        RESET: <code>{resCode}</code>
      </pre>
      {/* // ! DON'T PAY ATTENTION */}
      <hr />
    </div>
  );
};

export default Counter;
